# Kali-metasploit Docker Image
A docker image that has the [Metasploit Framework](https://github.com/rapid7/metasploit-framework) installed on a Kali image.  

## Building the Image
Build the docker image with the following command:

    $ docker build -t schwartz1375/docker-metasploit:latest -f ./Dockerfile .

## How to Launch the Metasploit Framework Docker Container
The most basic form is run the container and to start the msfconsole from a bash prompt:

    $ docker run -it schwartz1375/docker-metasploit

However, in most cases the -p parameter will be needed to expose the ports on which you expect to receive inbound connections from reverse shells.  In addition to make it a in a transient container (will disappear once you’ve exited) add the "--rm" option:

    $ docker run --rm -it -p 443:443 schwartz1375/docker-metasploit

One may also find it useful the next time you restart the app to pick up from where it left off, thus you will need to save some files to host.  To start from scratch, simple remove those directories (rm -rf ~/.msf4 /tmp/msf).

    $ docker run --rm -it -p 443:443 -v ~/.msf4:/root/.msf4 -v /tmp/msf:/tmp/data schwartz1375/docker-metasploit

For convenience the image also has the following packages installed:

  * [vim](http://www.vim.org)
  * [nmap](https://nmap.org)
  * [tmux](https://tmux.github.io)
